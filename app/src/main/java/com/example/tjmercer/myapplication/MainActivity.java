package com.example.tjmercer.myapplication;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView greetingLbl;
    Button nameViewButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nameViewButton = (Button) findViewById(R.id.btnNameView);
        nameViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(MainActivity.this, NameActivity.class);
                transition.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                transition.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(transition);
            }
        });

        Typeface customFont = Typeface.createFromAsset(getAssets(), "ComicSans.ttf");

        NameFetcher.getInstance(this);
        NameFetcher.getCurrentName();

        greetingLbl = (TextView) findViewById(R.id.greeting);

        greetingLbl.setTypeface(customFont);
        nameViewButton.setTypeface(customFont);
    }

    @Override
    protected void  onStart(){
        super.onStart();

        greetingLbl.setText(NameFetcher.greeting);

        nameViewButton.setText(NameFetcher.editLbl);
    }

    @Override
    public void onClick(View v){

    }
}
