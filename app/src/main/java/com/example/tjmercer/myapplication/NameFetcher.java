package com.example.tjmercer.myapplication;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;

import java.security.PrivateKey;

/**
 * Created by tjmercer on 6/1/16.
 */
public class NameFetcher {

    public static String currentName;
    public static String editLbl;
    public static String greeting;

    private static Context mContext;

    private static NameFetcher instance = new NameFetcher();

    public static NameFetcher getInstance(Context context) {
        mContext = context;
        return instance;
    }

    private NameFetcher(){
        // here you can directly access the Application context calling
    }

    public static void getCurrentName(){

        String defaultSt = mContext.getString(R.string.name_default);

        if (currentName == null) {

            String fileLocation = mContext.getString(R.string.name_file);
            SharedPreferences appPrefs = mContext.getSharedPreferences(fileLocation, Context.MODE_PRIVATE);

            String nameKey = mContext.getString(R.string.name_key);
            String name = appPrefs.getString(nameKey, defaultSt);

            currentName = name;
        }

        if (!currentName.equals(defaultSt)) {
            editLbl = mContext.getString(R.string.edit_name_button);
            greeting = mContext.getString(R.string.secondary_greeting)+" " +currentName;
        } else {
            editLbl = mContext.getString(R.string.create_name_button);
            greeting = mContext.getString(R.string.opening_question);
        }
    }

    public static void setCurrentName(String str) {
        String fileLocation = mContext.getString(R.string.name_file);
        SharedPreferences appPrefs = mContext.getSharedPreferences(fileLocation,Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = appPrefs.edit();

        String nameKey = mContext.getString(R.string.name_key);

        prefEditor.putString(nameKey, str);
        prefEditor.commit();

        currentName = str;

        getCurrentName();
    }
}
