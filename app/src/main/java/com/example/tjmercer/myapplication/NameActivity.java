package com.example.tjmercer.myapplication;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NameActivity extends AppCompatActivity implements View.OnClickListener {
    EditText nameText;
    Button okButton;
    Button cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        nameText = (EditText) findViewById(R.id.entName);
        nameText.setSelected(true);

        NameFetcher.getInstance(this);
        NameFetcher.getCurrentName();

        Typeface customFont = Typeface.createFromAsset(getAssets(), "ComicSans.ttf");

        okButton = (Button) findViewById(R.id.okbtn);
        okButton.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameText.getText().toString();

                if (!name.equals(NameFetcher.currentName) && !name.isEmpty()){
                    NameFetcher.setCurrentName(name);
                } else if (name.isEmpty()){
                    Toast.makeText(NameActivity.this, "You have to type a name", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Toast.makeText(NameActivity.this, "That's the same name", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent transition = new Intent(NameActivity.this, MainActivity.class);
                transition.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(transition);
                finish();
            }
        });

        cancelButton = (Button) findViewById(R.id.cancelbtn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent transition = new Intent(NameActivity.this, MainActivity.class);
                transition.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(transition);
                finish();
            }
        });

        nameText.setTypeface(customFont);
        TextView nameLbl = (TextView) findViewById(R.id.nameLbl);
        nameLbl.setTypeface(customFont);
        okButton.setTypeface(customFont);
        cancelButton.setTypeface(customFont);

        if (!NameFetcher.currentName.isEmpty()){
            nameText.setHint(NameFetcher.currentName);
        }
    }

    @Override
    public void onClick(View v) {

    }

}